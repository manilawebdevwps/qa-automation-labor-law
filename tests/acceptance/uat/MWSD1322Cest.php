<?php
namespace uat;
use \WebGuy;

class MWSD1322Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkWebmaster(WebGuy $I) {
        $I->amOnPage('/');
        $I->wantTo('check Bing Webmaster code');
        $I->canSeeElementInDOM('/html/head/meta[4]');
    }

}