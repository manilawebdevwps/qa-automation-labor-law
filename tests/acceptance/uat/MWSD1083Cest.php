<?php
namespace uat;
use \WebGuy;

class MWSD1083Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckBBBLogo(WebGuy $I) {
        $I->wantTo('To check if BBB logo is appearing');
        $I->amOnPage('/');
        $I->expectTo('See BBB Logo in Header');
        $I->canSeeElement('.topcartLinks .toprtLinks img ');
        $I->expectTo('See BBB Logo in Footer');
        $I->canSeeElement('#ccimages > li:nth-child(2) > img');
    }

}