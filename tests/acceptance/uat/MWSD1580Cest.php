<?php
namespace uat;
use \WebGuy;

class MWSD1580Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function llpoCheckout(WebGuy $I) {
        $I->wantToTest('LLPO credit card tokenization');
        $I->expectTo('see LLPO tokenization');
        $I->amOnPage('/new-york-labor-law-poster/');
        $I->maximizeWindow();
        $I->fillField('.qtyText', '1');
        #$I->click('.btnAddtoCart');
        $I->click('form input[name=submit]');
        $I->wait(3);
        $I->seeInCurrentUrl('/cart/');
        $I->click('//img[@src="http://207.97.211.132/laborlaw/media/images/btn-checkout.jpg"]');
        $I->seeInCurrentUrl('/account-login/');
        $I->wait(3);
        #$I->click('input#rbtOld.radioAlign');
        $I->click('//*[@id="rbtOld"]');
        $I->fillField('form input[name=email]','lilybeth_ranada@bradycorp.com');
        $I->fillField('input[name="cpassword"]','lilybeth');
        $I->wait(3);
        #$I->click('input.btnlft');
        $I->click('//*[@id="wrapperInside"]/div[4]/div[3]/div/div[1]/div/div[3]/div[2]/form/table/tbody/tr[4]/td[2]/input[1]');
        $I->wait(5);
        $I->seeInCurrentUrl('/billing-shipping/');
        $I->click('.btnSecond > input:nth-child(1)');
        $I->wait(3);
        $I->seeInCurrentUrl('/order-complete');
        $I->checkOption('#UPC');
        $I->checkOption('#creditcard');
        $I->selectOption('cardtype','VISA');
        $I->fillField('input[name="cardnumber"]','4111111111111111');
        $I->selectOption('form select[name=exp_Month]','12');
        $I->selectOption('form select[name=exp_Year]','2024');
        $I->fillField('form input[name=cardcode]','123');
        $I->checkOption('#termscondition');
        #$I->click('.chkoutBtn');
        $I->click('.btnSecond > input:nth-child(1)');
    }

    public function checkTokens(WebGuy $I) {
        $I->wantToTest('LLPO admin token');
        $I->expectTo('see tokens');
        $I->amOnPage('/admin/login.php');
        $I->maximizeWindow();
        $I->fillField('#username', 'pcadmin');
        $I->fillField('#password', 'pc@123');
        $I->click('input[type="image"]');
        $I->wait(5);
        $I->click('img[alt="Orders"]');
        $I->click('#chk_all');
        $I->click('input.editBtn');
        $I->see('//form[@id="frmgroups"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[20]/td[2]','-');
    }




}